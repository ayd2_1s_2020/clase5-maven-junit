package com.usac;
import java.util.*;
public class T4_200018283{
	
public T4_200018283() {		

	}
	
public double HPermimetro_circulo(double radio,double pi) {		
	 double perimetro=pi*(radio*radio);
	 return perimetro;
	}
	
public double HArea_circulo(double radio,double PI)  {		
	 double Area=2*PI*radio;
	 return Area;
	}
	
public int HFibonacci(int numero)  {		
	if (numero>1){
		   return HFibonacci(numero-1) + HFibonacci(numero-2);  
		}
		else if (numero==1) {  
			return 1;
		}
		else if (numero==0){  
			return 0;
		}
		else{ 
			System.out.println("Ingresar un tamaño mayor o igual a 1");
			return -1; 
		}
	}
public int HFactorial(int numero)  {		
	  if(numero == 0){
				return 1;
			}
			else
				return numero * HFactorial(numero-1);
		}
	
	
public boolean HPalindroma(String entrada){
        entrada=entrada.replace(" ", "");
        entrada=entrada.replace(",", "");
        entrada=entrada.replace(".", "");
        System.out.print(entrada);
        int fin = entrada.length()-1;
        int ini=0;
        boolean espalindroma=true;
        
        while(ini < fin){
            if(entrada.charAt(ini)!=entrada.charAt(fin)){
                espalindroma=false;
            }
        ini++;
        fin--;
        }
   return espalindroma;
		}			
}


