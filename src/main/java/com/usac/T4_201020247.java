package com.usac;

public class T4_201020247 {

    public T4_201020247(){
        System.out.println("Clase T4_201020247");
    }

	public double PerimetroCirculo(double radio, double pi){

		double perimetro = 2 * pi * radio;
		return perimetro;

	}

	public double AreaCirculo(double radio, double pi){

		double area = radio * radio *pi  ;
		return area;

	}

	public int fibonacci(int n){

	    if (n>1){
	       return fibonacci(n-1) + fibonacci(n-2);
	    }
	    else if (n==1) {
	        return 1;
	    }
	    else if (n==0){
	        return 0;
	    }
	    else{
	        System.out.println("Debes ingresar un tamaño mayor o igual a 1");
	        return -1; 
	    }
	}

	public int factorial (int numero) {
	  
		if (numero==0){
			return 1;
		}else{
			return numero * factorial(numero-1);
		}
    }
    
    public boolean espalindromo(String cadena) {
        char[] cad = cadena.toCharArray();

        int i = 0, j = cad.length - 1;

        while (i < j) {

            if (cad[i] != cad[j])
                return false;

            i++;
            j--;
        }

        return true;
    }

}
