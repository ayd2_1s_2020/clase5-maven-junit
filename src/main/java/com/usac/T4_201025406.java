package com.usac;

    public class T4_201025406 {
    
    private String perimetro;
    private String area;
    private double fibonacci;
    private int factorial;
    private boolean palindroma;

    //Constructor 
    public T4_201025406(){
    
    }

    //Calcular Perimetro
    public String perimetro(double radio, String unidad)
    {
        perimetro = 2 * Math.PI * radio + " " + unidad;
        return perimetro;
    }

    //Calcular Area
    public String area(double radio, String unidad)
    {
        area = Math.PI * Math.pow(radio,2) + " " + unidad + "^2";   
        return area;
    }
    
    //Calcular Fibonacci
    public double fibonacci(double numero)
    {
        
        if (numero == 0 ){  
            return 0;
        }
        
        if (numero == 1) { 
            return 1;
        }
        
        if (numero > 1){
       
            return fibonacci(numero - 1) + fibonacci(numero - 2); 
        }
        return fibonacci;
   
    }
      
    
    //Calcular Factorial
    public double factorial(double numero)
    {
       
        if(numero == 0){
            return 1 ;
        }
        else{
            return  numero * factorial(numero-1);
        }
    }
     
    //Palabra Palindroma (true or false)
    public Boolean palindroma(String palabra)
    {   
        int inicio = 0;
        int fin = palabra.length()-1;
        palindroma = true;
               
        while ((inicio<fin) && (palindroma)){		
            if (palabra.charAt(inicio)== palabra.charAt(fin)){				
		inicio++;
		fin--;
            } else {
		palindroma = false;
            }
        }
        
        return palindroma;
    }

  
}
