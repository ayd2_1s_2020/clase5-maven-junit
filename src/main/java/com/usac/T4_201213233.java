package com.usac;

public class T4_201213233 {

    public T4_201213233() {
        System.out.println("Clase T4_201213233");
    }

    public double obtenerPerimetro(double radio, String parametro2) {
        System.out.println("radio:" + radio);
        System.out.println("parametro 2: " + parametro2);
        return (2 * Math.PI * radio);
    }

    public double obtenerArea(double radio, String parametro2) {
        System.out.println("radio:" + radio);
        System.out.println("parametro 2: " + parametro2);
        return (Math.PI * Math.pow(radio, 2));
    }

    public int obtenerFibonacci(int n) {
        if (n == 1) {
            return 1;
        } else if (n == 0) {
            return 0;
        } else if (n > 1) {
            return obtenerFibonacci(n - 1) + obtenerFibonacci(n - 2);
        } else {
            System.out.println("Fibonacci utiliza numeros enteros positivos.");
            return -1;
        }
    }

    public int obtenerFactorial(int n) {
        if (n < 1) {
            return 1;
        }
        return n * obtenerFactorial(n - 1);
    }

    public boolean obtenerPalindroma(String word) {
        char[] cad = word.toCharArray();

        int i = 0, j = cad.length - 1;

        while (i < j) {

            if (cad[i] != cad[j])
                return false;

            i++;
            j--;
        }

        return true;
    }

}
