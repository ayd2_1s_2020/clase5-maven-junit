package com.usac;

/**
 * Hello world!
 *
 */
public class T4_201220159 
{
    //Clase constructor 
    public T4_201220159()
    {
        //Diego Ahtohil Noj Armira 
    }

    public double perimetro(double radio, double parametro2)
    {
        //Permitro de un circulo =radio*2*pi
        return radio*2*3.1416;
    }

    public double area(double radio, double parametro2)
    {
        //Area de un circulo = pi*radio*radio
        return 3.1416*radio*radio;
    }

    public int fibonacci(int parametro)
    {
        if (parametro>1){
        return fibonacci(parametro-1) + fibonacci(parametro-2);  //función recursiva
        }
        else if (parametro==1) {  // caso base
            return 1;
        }
        else if (parametro==0){  // caso base
            return 0;
        }
        else{ //error
            System.out.println("Debes ingresar un tamaño mayor o igual a 1");
            return -1; 
        }
    }

    public int factorial (int numero) 
    {
        if (numero==0)
          return 1;
        else
          return numero * factorial(numero-1);
    }

    public boolean palindroma(String palabra)
    {
        int ini=0;
        int cant=palabra.length()-1;
        boolean auxError=false;

        while ((ini<cant) && (!auxError)){
			
            if (palabra.charAt(ini)==palabra.charAt(cant)){				
                ini++;
                cant--;
            } else {
                auxError = true;
            }
        }
        return !auxError;
    }

    public int clear(){
        return 0;
    }

}
