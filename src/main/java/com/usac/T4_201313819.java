package com.usac;

public class T4_201313819 {
    public T4_201313819() {
        System.out.println("Clase T4_201313819");
    }

    public double Perimetro_Circulo(double radio, double pi) {
        return (2.0 * pi * radio);
    }

    public double Area_Circulo(double radio, double pi) {
        return pi * Math.pow(radio, 2);
    }

    public int fibonacci(int n) {
        // CASO BASE, si es cero devuelve un cero
        // Puedes poner n<=0 tamvien para incluir negativos
        if (n == 0) {
            return 0;
            // CASO BASE, si es 1 devuelve un 1
        } else if (n == 1) {
            return 1;
        } else {
            // Hago la suma
            return fibonacci(n - 1) + fibonacci(n - 2);
        }
    }

    public int factorial(int numero) {
        if (numero == 0) {
            return 1;
        } else {
            return numero * factorial(numero - 1);
        }
    }

    public boolean espalindromo(String cadena) {
        boolean valor = true;
        int i, ind;
        String cadena2 = "";
        // quitamos los espacios
        for (int x = 0; x < cadena.length(); x++) {
            if (cadena.charAt(x) != ' ')
                cadena2 += cadena.charAt(x);
        }
        // volvemos a asignar variables
        cadena = cadena2;
        ind = cadena.length();
        // comparamos cadenas
        for (i = 0; i < (cadena.length()); i++) {
            if (cadena.substring(i, i + 1).equals(cadena.substring(ind - 1, ind)) == false) {
                // si una sola letra no corresponde no es un palindromo por tanto
                // sale del ciclo con valor false
                valor = false;
                break;
            }
            ind--;
        }
        return valor;
    }
}