/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usac;

/**
 *
 * @author Jorge
 */
public class T4_201314285 {

    public T4_201314285() {
        System.out.println("Tarea numero 4 \n prueba de maven \n test unitarios \n Jorge Veliz 201314285");
    }
    
    /**
     * Metodo que devuelve el perimetro de un circulo
     * @param radio = Radio del circulo
     * @param pi = numero Pi ingresado
     * @return = Perimetro del circulo en double
     */
    public double PerimetroCirculo(double radio, double pi){
        return 2 * pi * radio;
    }

    /**
     * Metodo que devuelve el area de un circulo
     * @param radio = Radio del circulo
     * @param pi = numero Pi ingresado
     * @return = Area del circulo en double
     */
    public double AreaCirculo(double radio, double pi){
        return pi * Math.pow(radio, 2);
    }

    /**
     * Metodo que calcula la serie fibonnaci para un numero dado
     * @param n = numero a quien aplicarle la serie de fibonacci
     * @return = resultado de la serie 
     */
    public int fibonacci(int n){

        if (n>1){
           return fibonacci(n-1) + fibonacci(n-2);
        }
        else if (n==1) {
            return 1;
        }
        else if (n<=0){
            return 0;
        }
        return 0;
    }

    /**
     * Calculo de factorial para un numero dado
     * @param numero = Numero a quien aplicarle un factorial
     * @return  = Factorial de un numero
     */
    public double factorial (double numero) {
        double resultado = (numero==0)? 1 : numero * factorial(numero-1);
        return resultado;
    }

    /**
     * Verificacion de una cadena palindroma
     * @param cadena = cadena a evaluar
     * @return = Booleano de verificación de cadena palindromo
     */
    public boolean espalindromo(String cadena){
            boolean valor=true;
            int i,ind;
            String cadena2="";

            for (int x=0; x < cadena.length(); x++) {
                    if (cadena.charAt(x) != ' ')
                    cadena2 += cadena.charAt(x);
            }

            cadena=cadena2;
            ind=cadena.length();

            for (i= 0 ;i < (cadena.length()); i++){
                    if (cadena.substring(i, i+1).equals(cadena.substring(ind - 1, ind)) == false ) {
                    valor=false;
                    break;
                    }
                    ind--;
            }
            return valor;
    }
}
