package com.usac;

public class T4_201314646 {



	public double PerimetroCirculo(double radio, double pi){

		double perimetro = 2 * pi * radio;
		return perimetro;

	}

	public double AreaCirculo(double radio, double pi){

		double perimetro =pi * Math.pow(radio, 2);
		return perimetro;

	}

	public int fibonacci(int n){

	    if (n>1){
	       return fibonacci(n-1) + fibonacci(n-2);
	    }
	    else if (n==1) {
	        return 1;
	    }
	    else if (n==0){
	        return 0;
	    }
	    else{
	        System.out.println("Debes ingresar un tamaño mayor o igual a 1");
	        return -1; 
	    }
	}

	public double factorial (double numero) {
	  
		if (numero==0){
			return 1;
		}else{
			return numero * factorial(numero-1);
		}
	}

	public boolean espalindromo(String cadena){
		boolean valor=true;
		int i,ind;
		String cadena2="";
		
		for (int x=0; x < cadena.length(); x++) {
			if (cadena.charAt(x) != ' ')
			cadena2 += cadena.charAt(x);
		}
			
		cadena=cadena2;
		ind=cadena.length();
		
		for (i= 0 ;i < (cadena.length()); i++){
			if (cadena.substring(i, i+1).equals(cadena.substring(ind - 1, ind)) == false ) {
			valor=false;
			break;
			}
			ind --;
		}
		return valor;
	}

}