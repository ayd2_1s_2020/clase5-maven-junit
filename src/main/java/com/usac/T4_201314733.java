package com.usac;

public class T4_201314733 {
        public T4_201314733(){
        //constructor
    }
    
    public double perimetroCirculo(double radio, double pi){
        return 2 * pi * radio;
    }
    
    public double areaCirculo(double radio, double pi){
        return pi * Math.pow(radio, 2);
    }
    
    
    public int fibonacci(int n){
        if (n>1){
           return fibonacci(n-1) + fibonacci(n-2);  
        }else if (n==1) {  
            return 1;
        }else if (n==0){  
            return 0;
        }else{
            return -1; 
        }
    }
    
    public int factorial(int numero) {
        if (numero==0)
          return 1;
        else
          return numero * factorial(numero-1);
    }
    
    public boolean palindromo(String palabra){
        int inicio = 0;
        int fin = palabra.length() -1;
        boolean error = false;
		palabra = palabra.replace(" ","");
        while ((inicio<fin) && (!error)){		
            if (palabra.charAt(inicio)==palabra.charAt(fin)){				
                    inicio++;
                    fin--;
            } else {
                    error = true;
            }
        }
        if(error){
            return false;
        }else{
            return true;
        }
    }
}
