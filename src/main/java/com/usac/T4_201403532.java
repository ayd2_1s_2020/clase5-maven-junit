/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usac;

/**
 *
 * @author chicas
 */
public class T4_201403532 {

    /**
     * Constructor 201403532
     */
    public T4_201403532() {
        System.out.println("201403532");
    }

    /**
     * Calcular el perimetro de un circulo con el radio de parametro
     *
     * @param radio - double
     * @return - double con perimetro
     */
    public double calcularPerimetroCirculo(double radio) {
        return 2 * 3.14 * radio;
    }

    /**
     * Calcular el area de un circulo pasar radio como parametro
     *
     * @param radio - double
     * @return - double de area de circulo
     */
    public double calcularAreaCirculo(double radio) {
        return 3.14 * radio * radio;
    }

    /**
     * Serie de fibonacci
     *
     * @param entero - int
     * @return
     */
    public int calcularSerieFibonacci(int entero) {
        if (entero <= 1) {
            return entero;
        }
        return calcularSerieFibonacci(entero - 1) + calcularSerieFibonacci(entero - 2);
    }

    /**
     * Calcular factorial
     *
     * @param numero - int
     * @return
     */
    public int calcularFactorial(int numero) {
        if (numero == 0) {
            return 1;
        }

        return numero * calcularFactorial(numero - 1);
    }

    /**
     * Metodo boolean, para determinar si es un palindromo
     *
     * @param cadena
     * @return
     */
    public boolean esPalindromo(String cadena) {
        int i = 0, j = cadena.length() - 1;

        while (i < j) {

            if (cadena.charAt(i) != cadena.charAt(j)) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }

}
