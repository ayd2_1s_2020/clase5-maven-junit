/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usac;

/**
 *
 * @author juanr
 */
public class T4_201403733 {
    
    public T4_201403733(){
        System.out.println("201403733");
    }
    //PERIMETRO DE UN RECTANGULO
    public double parametro_rectangulo(double base, double altura){
        return 2*base + 2*altura;
    }
    //AREA DE UN RECTANGULO
    public double area_triangulo(double base, double altura){
        return (base*altura)/2;
    }
    //EVALUA SI EL NUMERO ES PRIMO
    public boolean es_primo(int num){
        if (num%2==0){
            return false;
        } 
        for(int i=3;i<=Math.sqrt(num);i+=2) { 
            if(num%i==0) 
                return false; 
        } 
        return true;  
    }
    //EVALUAR VOLUMEN DE UNA ESFERA BASADO SU RADIO
    public double volumen_esfera(double radio){
        double coeficiente= 4/3 * Math.PI;
        return (coeficiente * Math.pow(radio, 3));
    }
    
    //DEVUELVE TRUE O FALSE SI EL NUMERO EN CADENA ES ENTERO
    public boolean es_entero(String numero){
        boolean res = true;
        try{
            int num = Integer.parseInt(numero);
        }
        catch(Exception e){
            res=false;
        }
        return res;
    }
}
