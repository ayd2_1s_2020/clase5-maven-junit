package com.usac;

public class T4_201408580{

    private int key=8899;

    public T4_201408580(){
        System.out.println("201408580");
    }
    /** 
     * Calcular la suma de los valores ascii de caracteres de una palabra
     * @param cadena
     * @return 
    */
    public int getsumAscii(String cadena){

        char items[] = cadena.toCharArray();
        int sumAscii = 0;
        for(int i = 0; i< items.length; i++){
            sumAscii+=items[i];
        }
        return sumAscii;
    }

    /**
     * Calcular el perimetro de un rombo 
     * @param lado
     * @return
     */
    public int getPerimeterRhombus(int lado){
        return 4*lado;
    }

    /**
     * Calcular el area de un rombo, dados sus diagonales
     * @param diagonal1
     * @param diagonal2
     * @return 
     */
    public int getAreaRhombus(int diagonal1, int diagonal2){
        return (diagonal1*diagonal2)/2;
    }

    /**
     * Calcular el volumen de un cono, dados su altura y radio
     * @param altura
     * @param radio
     * @return 
     */
    public int getVolumeCone(int altura, int radio){
        return (Math.PI*Math.pow(radio,2)*altura);
    }

    /**
     * Metodo que calcula si la llave concuerda con la llave interna
     * @param entrada
     * @return 
     */
    public boolean getcompare(int entrada){
        return entrada ==this.key;
    }


}