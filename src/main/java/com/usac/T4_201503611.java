package com.usac;

public class T4_201503611 {

    private String nombreCompleto;
    private int edad;

    public T4_201503611() {
        System.out.println("Clase T4_201503611");
    }

    public String getNombre(String nombre, String apellido) {
        this.nombreCompleto = nombre.toLowerCase() + " " + apellido.toLowerCase();
        return this.nombreCompleto;
    }

    public int getEdad(int añoNacimiento) {
        this.edad = 2020 - añoNacimiento;
        return this.edad;
    }

    public boolean esPar(int num) {
        return num % 2 == 0;
    }

    public boolean esImpar(int num) {
        return num % 2 != 0;
    }

    public Boolean esPrimo(int num) {
        for(int i = 2; i <= num/2; ++i)
        {
            if(num % i == 0)
            {
                return true;
            }
        }
        return false;
    }

}
