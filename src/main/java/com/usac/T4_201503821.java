package com.usac;

public class T4_201503821{

    public T4_201503821(){
        System.out.println("201503821");
    }

    public double calcularPerimetroCirculo(double r){
        return 2 * 3.14 * r;
    }

    public double calcularAreaCirculo(double r){
        return 3.14 * r * r;
    }

    public int calcularFibonacci(int n){
        if (n <= 1) {
       		return n; 
        }
    	return calcularFibonacci(n-1) + calcularFibonacci(n-2); 
    }

    public int calcularFactorial(int n){
        if (n == 0) {
        	return 1;
        }
          
        return n*calcularFactorial(n-1); 
    }


    public boolean esPalindromo(String str){
        int i = 0, j = str.length() - 1; 

        while (i < j) { 
  
            if (str.charAt(i) != str.charAt(j)) 
                return false; 
  
            i++; 
            j--; 
        } 
  
        return true;
    }

}
