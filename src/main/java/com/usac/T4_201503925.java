/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usac;
import java.util.ArrayList;
/**
 *
 * @author Sergio
 */
public class T4_201503925 {
    
    /**
     * Constructor
     */
    public T4_201503925(){
    }
    /**
     * 
     * @param base
     * @param altura
     * @return volumen de una piramide 
     */
    public double calcularVolumenPiramida(double base, double altura){
        return (base*altura)/3;
    }
    
    /**
     * 
     * @param baseInferior
     * @param baseSuperior
     * @param altura
     * @return area de un trapecio 
     */
    public double calcularAreaTrapecio(double baseInferior, double baseSuperior, double altura){
        return ((baseInferior+baseSuperior)*altura)/2;
    }
    
    /**
     * 
     * @param lista tipo ArrayList de doubles
     * @return promedio de la lista recibida como parametro
     */
    public double calcularPromedio(ArrayList<Double> lista){
        double total=0;
        
        for (int i = 0; i < lista.size(); i++) {
            total+=lista.get(i);
        }

        return total/lista.size();
    }
    
    /**
     * 
     * @param numero
     * @return true si un numero es primo, false si el numero no es primo 
     */
    public boolean numeroPrimo(int numero){
        int contador = 2;
        
        while (contador!=numero){
          if (numero % contador == 0)
            return false;
          contador++;
        }
        return true;
    }
    
    public int contarVocalesEnUnaFrase(String cadena){
        int contador=0;
        for (int i = 0; i < cadena.length(); i++) {
            if(cadena.charAt(i)=='a'||cadena.charAt(i)=='e'||cadena.charAt(i)=='i'||cadena.charAt(i)=='o'||cadena.charAt(i)=='u'
               ||cadena.charAt(i)=='A'||cadena.charAt(i)=='E'||cadena.charAt(i)=='I'||cadena.charAt(i)=='O'||cadena.charAt(i)=='U'){
                contador++;
            }
        }
        
        return contador;
    }
    
    
}
