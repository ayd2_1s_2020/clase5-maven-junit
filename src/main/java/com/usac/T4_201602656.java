/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usac;

/**
 *
 * @author luisf
 */
class T4_201602656 {

    public T4_201602656() {
        System.out.println("Desarrollado por Luis Fernando Lizama - 201602656");
    }

    //Is Prime
    // Devuelve true si el parametro es un numero primo
    public boolean isPrime(int num) {
        boolean flag = false;
        for (int i = 2; i <= num / 2; ++i) {
            // condition for nonprime number
            if (num % i == 0) {
                flag = true;
                break;
            }
        }
        return !flag;
    }

    //Sumatoria    
    // Como un factorial, pero con sumatorias
    public int sumatoria(int t) {
        if (t <= 0) {
            return 0;
        }
        return t + sumatoria(t - 1);
    }

    //Area triangulo
    public double getTriangleArea(int base, int heigth) {
        return 0.5 * base * heigth;
    }

    //esImPar
    //Determina si un numero es impar
    public boolean isOdd(int number) {
        if (number % 2 == 0) {
            return false;
        }
        return true;
    }

    //vowels
    //Detecta cuantas vocales tiene una palabra
    public int vowels(String cadena) {
        int t = 0;
        char[] cad = cadena.toLowerCase().toCharArray();
        for (char c : cad) {
            switch (c) {
                case 'a':
                    t++;
                    break;
                case 'e':
                    t++;
                    break;
                case 'i':
                    t++;
                    break;
                case 'o':
                    t++;
                    break;
                case 'u':
                    t++;
                    break;
            }
        }
        return t;
    }

}
