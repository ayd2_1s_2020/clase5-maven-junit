package com.usac;

public class T4_201602822 {

    public T4_201602822() {
        System.out.println("201602822");
    }

    /**
     * Retorna el área de un rectángulo, dado el ancho y altura
     * 
     * @param width
     * @param height
     * @return
     */
    public double getRectangleArea(double width, double height) {
        return width * height;
    }

    /**
     * Retorna el volumen de una esfera, dado su radio
     * 
     * @param radio
     * @return
     */
    public double getSphereVolume(double radio) {
        return (4 / 3) * Math.PI * Math.pow(radio, 3);
    }

    /**
     * Retorna si un número es par o no
     * 
     * @param num
     * @return
     */
    public boolean isPair(int num) {
        return num % 2 == 0;
    }

    /**
     * Retorna si una palabra es pentavólica, es decir, si tiene todas las vocales, dado una palabra
     * @param word
     * @return
     */
    public boolean isPentavolicWord(String word) {
        word = word.toLowerCase();
        word = word.replace("á", "a").replace("é", "e").replace("í", "i").replace("ó", "o").replace("ú", "u");

        if (word.contains("a") && word.contains("e") && word.contains("i") && word.contains("o")
                && word.contains("u")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Retorna el valor de la función de ackermann dado 2 números naturales
     * 
     * @param m
     * @param n
     * @return
     */
    public long getAckermann(long m, long n) {
        if (m == 0)
            return n + 1;
        if (n == 0)
            return getAckermann(m - 1, 1);
        return getAckermann(m - 1, getAckermann(m, n - 1));
    }
}