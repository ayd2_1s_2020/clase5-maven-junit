package com.usac;

public class T4_201603166{

    public T4_201603166(){
        System.out.println("201603166");
    }

    public String getPerimeterCuadrado(double lado,String unit){
        System.out.println("lado:" + lado);
        System.out.println("Unidades: " + unit);
        return (4*lado) + "  " + unit;
    }

    public String getAreaCuadrado(double lado,String unit){
        System.out.println("lado:" + lado);
        System.out.println("Unidades: " + unit);
        return (Math.pow(lado,2)) + "  " + unit + "^2";
    }

    public boolean impar (int numero){
     if (numero==0)
      return false;
     else
      return par(numero-1);
    }

    public boolean par (int numero){
     if (numero==0)
      return true;
     else
      return impar(numero-1);
    }
    


    public boolean PalabraTresLetras(String word){
        

        return (word.length()==3)?true:false;
    }

}