package com.usac;

public class T4_201612226{

    public T4_201612226(){
        System.out.println("Clase T4_201612226");
    }

    public String getPerimeter(double radio,String unit){
        return (2*Math.PI*radio) + "  " + unit;
    }

    public String getArea(double radio,String unit){
        return (Math.PI*Math.pow(radio,2)) + "  " + unit + "^2";
    }

    public int getFibonacci(int n){
        if (n==1) {
            return 1;
        }else if (n==0){
            return 0;
        }else if (n > 1){
            return getFibonacci(n-1) + getFibonacci(n-2);
        }else{
            System.out.println("Fibonacci esta definido para los numeros enteros positivos.");
            return -1; 
        }
    }

    public int getFactorial(int n){
        return (n<=0)?1:n*getFactorial(n-1);
    }


    public boolean getPalindrome(String word){
        char [] cad = word.toCharArray();

        int i = 0;
        int size = cad.length-1;

        while(i <= (size/2)){
            if(cad[i] != cad[size-i]){
                return false;
            }
            i++;
        }

        return true;
    }

}