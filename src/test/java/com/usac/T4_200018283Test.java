package com.usac;
import static org.junit.Assert.assertEquals;
import org.junit.*;
import junit.framework.TestCase;
import org.junit.rules.TestWatcher;
import com.usac.T4_200018283;
/**
 * @author henry Fajardo
 */
public class T4_200018283Test {
public T4_200018283 test=new T4_200018283();
@Test
public void testHPerimetro_circulo(){
  double pi=3.1416;
  double radio=3;
  double resultado=test.HPermimetro_circulo(radio, pi);
  Assert.assertNotNull(resultado);
 }
 @Test
 public void testHArea_circulo(){
   double pi=3.1416;
   double radio=3;
   double resultado=test.HArea_circulo(radio, pi);
   Assert.assertNotNull(resultado);
  }
  @Test
  public void tesHFibonacci(){
    int numero=1;
    int resultado=test.HFibonacci(numero);
    Assert.assertNotNull(resultado);
   }

   @Test
   public void tesHFactorial(){
     int numero=5;
     int resultado=test.HFactorial(numero);
     Assert.assertNotNull(resultado);
    }

    @Test
    public void tesHPalindroma(){
      String palabra="aba";
      boolean resultado=test.HPalindroma(palabra);
      Assert.assertNotNull(resultado);
     }
   
  }