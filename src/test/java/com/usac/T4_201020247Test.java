package com.usac;

import junit.framework.TestCase;
import static org.junit.Assert.assertEquals;
import org.junit.*;

public class T4_201020247Test extends TestCase {
    T4_201020247 test = new T4_201020247();
    

    @Test
    public void PerimetroCirculoTest(){
        assertEquals(94.248, test.calcularPerimetroCirculo(15,3.1416));
    }

    @Test
    public void AreaCirculoTest(){
        assertEquals(706.86, test.calcularAreaCirculo(15,3.1416));
    }

    @Test
    public void fibonacciTest(){
        assertEquals(610, test.calcularFibonacci(15));
    }

    @Test
    public void factorialTest(){
        assertEquals(120, test.calcularFactorial(5));
    }

    @Test
    public void espalindromoTest(){
        assertEquals(true, test.esPalindromo("abba"));
    }

    @BeforeClass
    public static void beforeClass() {
        System.out.println("Empezando Test de Clase T4_201020247");
        test = new T4_201020247();
    }
    
    @Before
    public void before(){
        System.out.println("Empezando Test de la Funcion");
    }
    
    @AfterClass
    public static void afterClass() {
        System.out.println("Finalizando Test de la clase T4_201020247");
    }
    
    @After
    public void after(){
        System.out.println("Finalizando Test de la Funcion");
    }

    
}
