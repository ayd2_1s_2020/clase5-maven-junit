package com.usac;

import static org.junit.Assert.assertEquals;
import org.junit.*;
import junit.framework.TestCase;

public class T4_201025406Test extends TestCase {
    
    public T4_201025406Test() {
    }
    
    static T4_201025406Test tmp;

    @BeforeClass
    public static void beforeClass() {
        T4_201025406  tmp = new T4_201025406();
    }
    
    @Before
    public void before(){

        System.out.println("Iniciando");
    }
    
    @AfterClass
    public static void afterClass() {
        System.out.println("[x] Fin de la prueba");
    }
    
    @After
    public void after(){
        System.out.println("La prueba ha finalizado");
    }
    
    @Test
    public void perimetroTest(){
        assertEquals("18.84955592153876 cm", tmp.perimetro(3,"cm"));
    }

    @Test
    public void areaTest(){
        assertEquals("28.274333882308138 cm^2", tmp.area(3,"cm"));
    }

    @Test
    public void fibonacciTest(){
        assertEquals(8.0, tmp.fibonacci(6));
    }

    @Test
    public void factorialTest(){
        assertEquals(720.0, tmp.factorial(6));
    }

    @Test
    public void palindromaTest(){
        assertEquals(true, tmp.palindroma("reconocer"));
    }
    
}
