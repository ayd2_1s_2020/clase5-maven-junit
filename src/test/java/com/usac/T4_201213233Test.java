package com.usac;

import static org.junit.Assert.assertEquals;
import org.junit.*;
import junit.framework.TestCase;

public class T4_201213233Test extends TestCase {
    
    public T4_201213233Test() {
    }
    
    static T4_201213233 test;

    @BeforeClass
    public static void beforeClass() {
        System.out.println("Iniciando Test de la clase T4_201213233");
        test = new T4_201213233();
    }
    
    @Before
    public void before(){
        System.out.println("Iniciando Test de la Funcion");
    }
    
    @AfterClass
    public static void afterClass() {
        System.out.println("Terminando Test de la clase T4_201213233");
    }
    
    @After
    public void after(){
        System.out.println("Terminando Test de la Funcion");
    }
    

    @Test
    public void obtenerPerimetroTest(){
        System.out.println("obtenerPerimetro");
        assertEquals("50.2654824574", test.obtenerPerimetro(8,""));
    }

    @Test
    public void obtenerAreaTest(){
        System.out.println("obtenerArea");
        assertEquals("201.06192983", test.obtenerArea(8,""));
    }

    @Test
    public void obtenerFibonacciTest(){
        System.out.println("obtenerFibonacci");
        assertEquals(21, test.obtenerFibonacci(6));
    }

    @Test
    public void obtenerFactorialTest(){
        System.out.println("obtenerFactorial");
        assertEquals(40320, test.obtenerFactorial(8));
    }

    @Test
    public void getPalindromeTest(){
        System.out.println("obtenerPalindroma");
        assertEquals(true, test.obtenerPalindroma("abba"));
    }
    
}
