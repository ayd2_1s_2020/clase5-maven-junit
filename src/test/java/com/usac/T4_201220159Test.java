package com.usac;

import static org.junit.Assert.assertEquals;

import org.junit.*;


import junit.framework.TestCase;
import org.junit.rules.TestWatcher;


/**
 * HigherTest
 */
public class T4_201220159Test extends TestCase {

    static T4_201220159 h;


    @BeforeClass
    public static void before() {
        System.out.println("Before");
        h = new T4_201220159();
    }
 
    @Test
    public void testT4_201220159_perimetro() {
        h = new T4_201220159();
        System.out.println("Test perimetro");

        assertEquals( 12.5664,  h.perimetro(2,0));
        
        //assertEquals("la prueba no funciono", 25,  h.higherxy(25, 5));
    }

    @Test
    public void testT4_201220159_area() {
        h = new T4_201220159();
        System.out.println("Test area");

        assertEquals( 12.5664,  h.area(2,0));
        
        //assertEquals("la prueba no funciono", 25,  h.higherxy(25, 5));
    }

    @Test
    public void testT4_201220159_fibonacci() {
        h = new T4_201220159();
        System.out.println("Test fibonacci");

        assertEquals( 2,  h.fibonacci(2));
        
        //assertEquals("la prueba no funciono", 25,  h.higherxy(25, 5));
    }

    @Test
    public void testT4_201220159_factorial() {
        h = new T4_201220159();
        System.out.println("Test factorial");

        assertEquals( 120,  h.factorial(5));
        
        //assertEquals("la prueba no funciono", 25,  h.higherxy(25, 5));
    }


    @Test
    public void testT4_201220159_palindromo() {
        h = new T4_201220159();
        System.out.println("Test palindromo");

        assertEquals(true, h.palindroma("reconocer"));
        
        //assertEquals("la prueba no funciono", 25,  h.higherxy(25, 5));
    }


    @After
    public void after(){
        System.out.println("after()");
        h = new T4_201220159();
        h.clear();

    }

}