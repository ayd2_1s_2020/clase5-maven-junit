package com.usac;

import static org.junit.Assert.assertTrue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import junit.framework.TestCase;

/**
 * T4_201313819Test
 */
public class T4_201313819Test extends TestCase {
    T4_201313819 t4 = new T4_201313819();

    public T4_201313819Test() {
    }

    @Before
    public void antes() {
        System.out.println("Inicia test");
    }

    @After
    public void despues() {
        System.out.println("Finaliza test");
    }

    @BeforeClass
    public void antesAll() {
        System.out.println("Inicio test T4_201313819.");

    }

    @AfterClass
    public void despuesAll() {
        System.out.println("Fin test T4_201313819");
    }

    @Test
    public void test_Perimetro_Circulo() {
        int radio = 5;
        Assert.assertEquals("Result", (2.0 * Math.PI * radio), t4.Perimetro_Circulo(radio, Math.PI), 0);
    }

    @Test
    public void test_Area_Circulo() {
        int radio = 5;
        Assert.assertEquals("Result", Math.PI * Math.pow(radio, 2), t4.Area_Circulo(radio, Math.PI), 0);
    }

    @Test
    public void test_Fibonacci() {
        Assert.assertEquals("Result", 55, t4.fibonacci(10));
    }

    @Test
    public void test_Factorial() {
        Assert.assertEquals("Result", 3628800, t4.factorial(10));
    }

    @Test
    public void test_Palindromo() {
        assertTrue(t4.espalindromo("ama"));
    }
}