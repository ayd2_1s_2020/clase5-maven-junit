/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usac;

import static org.junit.Assert.assertTrue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import junit.framework.TestCase;

/**
 *
 * @author Jorge
 */
public class T4_201314285Test {
    T4_201314285 t4 = new T4_201314285();

    @BeforeClass
    public void beforeclass() {
        System.out.println("before class");

    }

    @AfterClass
    public void afterclass() {
        System.out.println("after class");
    }
    
     @Before
    public void before() {
        System.out.println("before");
    }

    @After
    public void after() {
        System.out.println("after");
    }
    
    @Test
    public void TestPerimetroCirculo() {
        double radio = 5.0;
        double pi = 3.14;
        Assert.assertEquals("Result", (2.0 * pi * radio), t4.PerimetroCirculo(radio, pi), 0);
    }

    @Test
    public void TestAreaCirculo() {
        double radio = 5.0;
        double pi = 3.14;
        Assert.assertEquals("Result", pi * Math.pow(radio, 2), t4.AreaCirculo(radio, pi), 0);
    }

    @Test
    public void TestFibonacci() {
        Assert.assertEquals("Result", 55, t4.fibonacci(10), 0);
    }

    @Test
    public void TestFactorial() {
        Assert.assertEquals("Result", 0, t4.factorial(1), 0);
    }

    @Test
    public void TestEspalindromo() {
        assertTrue(t4.espalindromo("ama"));
    }

}
