package com.usac;

import junit.framework.TestCase;
import static org.junit.Assert.assertEquals;
import org.junit.*;

public class T4_201314646Test extends TestCase {
    T4_201314646 test = new T4_201314646();

    @Test
    public void PerimetroCirculoTest(){
        assertEquals(62.832, test.calcularPerimetroCirculo(10,3.1416));
    }

    @Test
    public void AreaCirculoTest(){
        assertEquals(314.16, test.calcularAreaCirculo(10,3.1416));
    }

    @Test
    public void fibonacciTest(){
        assertEquals(55, test.calcularFibonacci(10));
    }

    @Test
    public void factorialTest(){
        assertEquals(3628800, test.calcularFactorial(10));
    }

    @Test
    public void espalindromoTest(){
        assertEquals(true, test.esPalindromo("anita lava la tina"));
    }
    
}
