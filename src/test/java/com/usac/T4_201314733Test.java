package com.usac;

import static org.junit.Assert.assertTrue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import junit.framework.TestCase;


public class T4_201314733Test extends TestCase {
    T4_201314733 t4 = new T4_201314733();

    public T4_201314733Test() {
    }

    @Before
    public void antes() {
        System.out.println("Inicia test");
    }

    @After
    public void despues() {
        System.out.println("Finaliza test");
    }

    @BeforeClass
    public void antesAll() {
        System.out.println("Inicio test T4_201314733.");

    }

    @AfterClass
    public void despuesAll() {
        System.out.println("Fin test T4_201314733");
    }

    @Test
    public void test_perimetroCirculo() {
        int radio = 5;
        Assert.assertEquals("Result", (2.0 * Math.PI * radio), t4.perimetroCirculo(radio, Math.PI), 0);
    }

    @Test
    public void test_areaCirculo() {
        int radio = 5;
        Assert.assertEquals("Result", Math.PI * Math.pow(radio, 2), t4.areaCirculo(radio, Math.PI), 0);
    }

    @Test
    public void test_Fibonacci() {
        Assert.assertEquals("Result", 55, t4.fibonacci(10));
    }

    @Test
    public void test_Factorial() {
        Assert.assertEquals("Result", 3628800, t4.factorial(10));
    }

    @Test
    public void test_Palindromo() {
        assertTrue(t4.palindromo("arriba la birra"));
    }
}
