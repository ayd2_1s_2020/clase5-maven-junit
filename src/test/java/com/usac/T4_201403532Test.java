/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usac;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author chicas
 */
public class T4_201403532Test {
    
    T4_201403532 prueba;
    
    public T4_201403532Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        this.prueba = new T4_201403532();
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void calcularPerimetroTest() {
        assertEquals(12.56, this.prueba.calcularPerimetroCirculo(2), 0.0f);
    }
    
    @Test
    public void calcularAreaTest() {
        assertEquals(12.56, this.prueba.calcularAreaCirculo(2), 0.0f);
    }
    
    @Test
    public void calcularFibonacciTest() {
        assertEquals(89, this.prueba.calcularSerieFibonacci(11));
    }
    
    @Test
    public void calcularFactorialTest() {
        assertEquals(24, this.prueba.calcularFactorial(4));
    }
    
    @Test
    public void esPalendromoTest() {
        assertTrue(this.prueba.esPalindromo("reconocer"));
    }
}
