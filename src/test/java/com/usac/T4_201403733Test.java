/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usac;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Assert;
import junit.framework.TestCase;
import static org.junit.Assert.*;

/**
 *
 * @author juanr
 */
public class T4_201403733Test extends TestCase {
    T4_201403733 T4 = new T4_201403733();
    
    public T4_201403733Test(){
        
    }
    
    @Before 
    public void before() { 
        System.out.println("Inicia Test"); 
    } 
 
    @After 
    public void after() { 
        System.out.println("Finaliza Test"); 
    } 
 
    @BeforeClass 
    public void beforeClass() { 
        System.out.println("Inicio Test T4_201403733"); 
 
    } 
 
    @AfterClass 
    public void afterClass() { 
        System.out.println("Fin Test T4_201403733"); 
    } 
    
    @Test
    public void test_parametro_rectangulo(){
        double base = 2;
        double altura = 3;
        Assert.assertEquals("Resultado",(2*base+2*altura),T4.parametro_rectangulo(2, 3));
    }
    
    
    @Test
    public void test_area_triangulo(){
        double base = 2;
        double altura = 3;
        Assert.assertEquals("Resultado",(base*altura)/2,T4.area_triangulo(2, 3));
    }
    
    @Test
    public void test_es_primo(){
        Assert.assertTrue(T4.es_primo(37));
    }
    
    @Test
    public void test_volumen_esfera(){
        int radio=5;
        Assert.assertEquals("Resultado",(4/3)* Math.PI * Math.pow(radio, 3),T4.volumen_esfera(5));
    }
    
    @Test
    public void test_es_entero(){
        Assert.assertFalse(T4.es_entero("314.25"));
    }
    
}
