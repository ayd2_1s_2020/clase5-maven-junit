package com.usac;

import static org.junit.Assert.assertTrue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import junit.framework.TestCase;


public class T4_201408580Test extends TestCase {
    T4_201408580 t4 = new T4_201408580();

    public T4_201408580Test() {
    }

    @Before
    public void before() {
        System.out.println("Inicia test");
    }

    @After
    public void after() {
        System.out.println("Finaliza test");
    }

    @BeforeClass
    public void beforeClass() {
        System.out.println("Inicio test T4_201408580.");

    }

    @AfterClass
    public void afterClass() {
        System.out.println("Fin test T4_201408580");
    }

    @Test
    public void test_suma_ascii() {
        String entrada = "hola";
        int salida = 420;
        assertEquals("Result", salida, t4.getsumAscii(entrada));
    }

    @Test
    public void test_perimetro_rombo() {
        int entrada = 5;
        int salida = 20;
        assertEquals("Result", salida, t4.getPerimeterRhombus(entrada));
    }

    @Test
    public void test_area_rombo() {
        int diagonal1 = 10;
        int diagonal2 = 5;
        int salida = (diagonal1*diagonal2)/2
        assertEquals("Result", salida, t4.getAreaRhombus(diagonal1,diagonal2));
    }

    @Test
    public void test_volumen_cono() {
        int radio=6;
        int altura = 10;
        int salida = Math.PI*Math.pow(radio,2)*altura;
        assertEquals("Result", salida, t4.getVolumeCone(altura,radio));
    }

    @Test
    public void test_comparar_llave_interna() {
        int key=8899;
        assertTrue(t4.getcompare(key));
    }
}