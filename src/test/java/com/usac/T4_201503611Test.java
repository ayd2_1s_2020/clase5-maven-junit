package com.usac;

import junit.framework.TestCase;
import static org.junit.Assert.assertEquals;
import org.junit.*;

public class T4_201503611Test extends TestCase {

    static T4_201503611 test;

  
    @Test
    public void obtenerNombreApellido(){
        assertEquals("Pavel Vasquez", test.getNombre("Pavel", "Vasquez"));
    }
    
    @Test
    public void calcularEdadConAño(){
        assertEquals(24, test.getEdad(1996));
    }
    
    @Test
    public void verificarImpar(){
        assertEquals(true, test.esImpar(1995));
    }
    
    @Test
    public void verificarPar(){
        assertEquals(true, test.esPar(1996));
    }
    
    @Test
    public void verificarPrimo(){
        assertEquals(false, test.esPrimo(1996));
    }
}
