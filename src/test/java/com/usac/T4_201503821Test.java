package com.usac;

import junit.framework.TestCase;
import static org.junit.Assert.assertEquals;
import org.junit.*;

public class T4_201503821Test extends TestCase {
    
    
    static T4_201503821 test;

    public T4_201503821Test() {
    
    }
    
    @Test
    public void calcularPerimetroCirculoTest(){
        assertEquals(628, test.calcularPerimetroCirculo(100));
    }

    @Test
    public void calcularAreaCirculoTest(){
        assertEquals(31400, test.calcularAreaCirculo(100));
    }

    @Test
    public void calcularFibonacciTest(){
        assertEquals(1, test.calcularFibonacci(2));
    }

    @Test
    public void calcularFactorialTest(){
        assertEquals(6, test.calcularFactorial(3));
    }

    @Test
    public void esPalindromoTest(){
        assertEquals(true, test.esPalindromo("ala"));
    }
    
}
