/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usac;

import static org.junit.Assert.assertEquals;
import org.junit.*;
import junit.framework.TestCase;
import java.util.ArrayList;


/**
 *
 * @author Sergio
 */
public class T4_201503925Test extends TestCase{
    T4_201503925 tarea4=new T4_201503925();
    
    @BeforeClass
    public static void antesAll(){
        System.out.println("----- Inicio Test 201503925------");
    }
    
    @AfterClass
    public static void despuesAll(){
        System.out.println("------ Fin Test --------");
    }
    
    @Before
    public void antes(){
         System.out.println("Inicia Test");
    } 
    
    @After
    public void despues(){
     System.out.println("Finaliza Test");
    }
    
    
    @Test
    public void testCalcularVolumenPiramida(){
        assertEquals(2,tarea4.calcularVolumenPiramida(2,3));
    }
    
    @Test
    public void testCalcularAreaTrapecio(){
        assertEquals(14,tarea4.calcularAreaTrapecio(4,3,4));
    }
    
    @Test
    public void testCalcularPromedio(){
        ArrayList<Double> lista=new ArrayList<Double>();
        lista.add(8.0);
        lista.add(4.0);
        lista.add(5.0);
        lista.add(8.0);
        lista.add(10.0);

        assertEquals(7.0,tarea4.calcularPromedio(lista));
    }
    
    @Test
    public void testNumeroPrimo(){
        assertEquals(true,tarea4.numeroPrimo(7));
    }
    
    @Test
    public void testContarVocalesEnUnaFrase(){
        assertEquals(6,tarea4.contarVocalesEnUnaFrase("tarea 4 prueba"));
    }
    
 
}
