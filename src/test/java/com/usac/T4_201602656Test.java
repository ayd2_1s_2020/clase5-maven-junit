/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.usac;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author luisf
 */
public class T4_201602656Test {
    
    public T4_201602656Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        System.out.println("Preparando la clase - Luis Fernando Lizama 201602656");
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        System.out.println("Starting function test");
        
    }
    
    @After
    public void tearDown() {
        System.out.println("test ended");
    }

    /**
     * Test of isPrime method, of class T4_201602656.
     */
    @Test
    public void testIsPrime() {
        System.out.println("isPrime");
        int num = 7;
        T4_201602656 instance = new T4_201602656();
        boolean expResult = true;
        boolean result = instance.isPrime(num);
        assertEquals(expResult, result);        
    }

    /**
     * Test of sumatoria method, of class T4_201602656.
     */
    @Test
    public void testSumatoria() {
        System.out.println("sumatoria");
        int t = 4;
        T4_201602656 instance = new T4_201602656();
        int expResult = 10;
        int result = instance.sumatoria(t);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getTriangleArea method, of class T4_201602656.
     */
    @Test
    public void testGetTriangleArea() {
        System.out.println("getTriangleArea");
        int base = 2;
        int heigth = 2;
        T4_201602656 instance = new T4_201602656();
        double expResult = 2;
        double result = instance.getTriangleArea(base, heigth);
        assertEquals(expResult, result, 0.0);        
    }

    /**
     * Test of isOdd method, of class T4_201602656.
     */
    @Test
    public void testIsOdd() {
        System.out.println("isOdd");
        int number = 8;
        T4_201602656 instance = new T4_201602656();
        boolean expResult = false;
        boolean result = instance.isOdd(number);
        assertEquals(expResult, result);
    }

    /**
     * Test of vowels method, of class T4_201602656.
     */
    @Test
    public void testVowels() {
        System.out.println("vowels");
        String cadena = "Luis";
        T4_201602656 instance = new T4_201602656();
        int expResult = 2;
        int result = instance.vowels(cadena);
        assertEquals(expResult, result);
        
    }
    
}
