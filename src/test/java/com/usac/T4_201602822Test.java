package com.usac;

import org.junit.*;
import static org.junit.Assert.*;

public class T4_201602822Test{
    public T4_201602822Test() {
    }
    
    static T4_201602822 test;

    @BeforeClass
    public static void beforeClass() {
        System.out.println("Testing T4_201602822");
        test = new T4_201602822();
    }
    @AfterClass
    public static void afterClass() {
        System.out.println("Finishing T4_201602822");
    }
    
    @Before
    public void before(){
        System.out.println("-Testing function");
    }
    
    @After
    public void after(){
        System.out.println("-Finishing function...");
    }
    
    @Test
    public void getRectagleAreaTest(){
        System.out.println("--getRectangleArea");
        assertEquals(12, test.getRectangleArea(3, 4), 0.0);
    }

    @Test
    public void getSphereVolumeTest(){
        System.out.println("--getSphereVolume");
        assertEquals(27, test.getSphereVolume(3), 0.0);
    }

    @Test
    public void isPairTest(){
        System.out.println("--isPair");
        assertEquals(true, test.isPair(0));
    }

    @Test
    public void isPentavolicWordTest(){
        System.out.println("--isPentavolicWord");
        assertEquals(true, test.isPentavolicWord("murciélago"));
    }

    @Test
    public void getAckermannTest(){
        System.out.println("--getAckermann");
        assertEquals(29, test.getAckermann(3, 2));
    }


}