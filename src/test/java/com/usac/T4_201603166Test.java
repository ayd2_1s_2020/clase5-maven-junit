package com.usac;

import static org.junit.Assert.assertEquals;
import org.junit.*;
import junit.framework.TestCase;

public class T4_201603166Test extends TestCase {
    
    public T4_201603166Test() {
    }
    
    static T4_201603166 test;

    @BeforeClass
    public static void beforeClass() {
        System.out.println("------------Iniciando Test de la clase T4_201603166------------");
        test = new T4_201603166();
    }
    
    @Before
    public void before(){
        System.out.println("###Iniciando Test de la Funcion");
    }
    
    @AfterClass
    public static void afterClass() {
        System.out.println("------------Terminando Test de la clase T4_201603166------------");
    }
    
    @After
    public void after(){
        System.out.println("###Terminando Test de la Funcion");
    }
    

    @Test
    public void getPerimeterTest(){
        System.out.println("getPerimeterCuadrado");
        assertEquals("20.0  m", test.getPerimeterCuadrado(5,"m"));
    }

    @Test
    public void getAreaTest(){
        System.out.println("getAreaCuadrado");
        assertEquals("25.0  m^2", test.getAreaCuadrado(5,"m"));
    }

    @Test
    public void getFibonacciTest(){
        System.out.println("Par");
        assertEquals(true, test.par(6));
    }

    @Test
    public void getFactorialTest(){
        System.out.println("Impar");
        assertEquals(true, test.impar(7));
    }

    @Test
    public void getPalindromeTest(){
        System.out.println("PalabraTresLetras");
        assertEquals(true, test.PalabraTresLetras("sol"));
    }
    
}