package com.usac;

import static org.junit.Assert.assertEquals;
import org.junit.*;
import junit.framework.TestCase;

public class T4_201612226Test extends TestCase {
    
    public T4_201612226Test() {
    }
    
    static T4_201612226 test;

    @BeforeClass
    public static void beforeClass() {
        System.out.println("------------Iniciando Test de la clase T4_201612226------------");
        test = new T4_201612226();
    }
    
    @Before
    public void before(){
        System.out.println("###Iniciando Test de la Funcion");
    }
    
    @AfterClass
    public static void afterClass() {
        System.out.println("------------Terminando Test de la clase T4_201612226------------");
    }
    
    @After
    public void after(){
        System.out.println("###Terminando Test de la Funcion");
    }
    

    @Test
    public void getPerimeterTest(){
        System.out.println("getPerimeter");
        assertEquals("18.84955592153876  m", test.getPerimeter(3,"m"));
    }

    @Test
    public void getAreaTest(){
        System.out.println("getArea");
        assertEquals("28.274333882308138  m^2", test.getArea(3,"m"));
    }

    @Test
    public void getFibonacciTest(){
        System.out.println("getFibonacci");
        assertEquals(8, test.getFibonacci(6));
    }

    @Test
    public void getFactorialTest(){
        System.out.println("getFactorial");
        assertEquals(720, test.getFactorial(6));
    }

    @Test
    public void getPalindromeTest(){
        System.out.println("getPalindrome");
        assertEquals(true, test.getPalindrome("reconocer"));
    }
    
}